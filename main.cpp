#include <iostream>

using namespace std;

struct node {
    node* next;
    int value;
};

class NodeUnavailableException: public exception {
    const char* what() const noexcept override {
        return "An unavailable or missing node was requested from a linked list.";
    }
};

class LinkedList {
    private:
    node* head;
    node* tail;

    public:
    LinkedList() {
        head = nullptr;
        tail = nullptr;
    }

    node* getHead() {
        return head;
    }

    node* getTail() {
        return tail;
    }

    int getLength() {
        if (head == nullptr)
            return 0;

        node* current = head;
        int counter = 0;

        while (current != nullptr) {
            counter++;
            current = current->next;
        }

        return counter;
    }

    int getItem(int position) {
        if (head == nullptr)
            throw NodeUnavailableException();

        node* current = head;
        int counter = 0;

        while (counter != position) {
            counter++;
            current = current->next;

            if (current == nullptr)
                throw NodeUnavailableException();
        }

        return current->value;
    }

    void append(int value) {
        node* newNode = new node;
        newNode->next = nullptr;
        newNode->value = value;

        if (head == nullptr) {
            head = newNode;
            tail = newNode;
        } else {
            tail->next = newNode;
            tail = newNode;
        }
    }

    void prepend(int value) {
        node* newNode = new node;
        newNode->next = head;
        newNode->value = value;

        head = newNode;
    }
};

void displayLinkedList(LinkedList* list) {
    node* temp = list->getHead();

    cout << "<===============>" << endl;
    cout << "Displaying a list of " << list->getLength() << " item(s)." << endl << endl;

    while (temp != nullptr) {
        cout << " - " << temp->value << endl;
        temp = temp->next;
    }

    cout << "<===============>";
}

int main() {
    auto list = new LinkedList();

    list->append(50);
    list->append(40);
    list->append(30);
    list->prepend(10);

    displayLinkedList(list);

    cout << "The item at position 2 is " << list->getItem(4);

    return 0;
}
